using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        Texture2D backgroundTexture;
        SpriteBatch Sprite;

        Platform platform;

        Vector2 vector;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here



            backgroundTexture = Content.Load<Texture2D>(@"Textures\mosaic1");
            Sprite = new SpriteBatch(graphics.GraphicsDevice);

            platform = new Platform(backgroundTexture, new Vector2(0, Window.ClientBounds.Height - 256), graphics);

            vector = new Vector2(0, Window.ClientBounds.Height - 256*(0.3f));
            Window.AllowUserResizing=true;


            base.Initialize();
        }

        
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);


            platform.Draw(0.3f, vector);
            
            

           
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }



    public class Matador
    {
        public Texture2D MatadorTexture;
        public Texture2D MatadorRunSpriteList;
        
        public Vector2 Position;
        public Vector2 Speed;

        public int HealthPoints;
        public int Damage;

    }

    public class Platform : Microsoft.Xna.Framework.Game
    {
        Texture2D PlatformTexture;
        Vector2 Size;
        Vector2 Position;
        public SpriteBatch PlatformSprite;
        GraphicsDeviceManager Device;

        public Platform(Texture2D texture,Vector2 position, GraphicsDeviceManager device)
        {
            Device = device;
            Position = position;
            PlatformTexture = texture;
            PlatformSprite = new SpriteBatch(Device.GraphicsDevice);
        }

        public void Draw(float Scale, Vector2 position)
        {
            PlatformSprite.Begin();
            for (int i=0; i < 10; i++)
            {
               
                PlatformSprite.Draw(PlatformTexture,
                   position,
                   null,
                   Color.White,
                   0.0f,
                   new Vector2(0, 0),
                   Scale,
                   SpriteEffects.None,
                   0.0f);

                position.X += 256 * Scale + 2;
            }

            PlatformSprite.End();
        }
    }
    
    
}
